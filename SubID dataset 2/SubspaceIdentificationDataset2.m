clear all
close all

%% load test data

load('RandomSquareAmpl_0_1Simtime_100.mat')
data_RandomSquareAmpl_0_1Simtime_100 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);           % save inputs and outputs as iddata object to be used in n4sid function
Ts_RandomSquareAmpl_0_1Simtime_100 = Ts;                                              % save sampling time to be used for plot
u_RandomSquareAmpl_0_1Simtime_100 = u1(:,2);                                          % save u to be used for plot to compare
y_RandomSquareAmpl_0_1Simtime_100 = y(:,2);                                           % save y to be used to calculate VAF

figure                                                                      % plot input and measured output data in one figure
plot(y1(:,1),y1(:,2));                                                      
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('RandomSquareAmpl_0_1Simtime_100.mat')

load('RandomSquareRampSimtime_100.mat')
data_RandomSquareRampSimtime_100 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);           % save inputs and outputs as iddata object to be used in n4sid function
Ts_RandomSquareRampSimtime_100 = Ts;                                              % save sampling time to be used for plot
u_RandomSquareRampSimtime_100 = u1(:,2);                                          % save u to be used for plot to compare
y_RandomSquareRampSimtime_100 = y(:,2);

% subplot(2,1,1)                                                                      % plot input and measured output data in one figure
% plot(y1(:,1),y1(:,2));                                                      
% hold on
% plot(y(:,1),y(:,2));
% legend('actuated disc','output disc')
% title('output')
% xlabel('t [s]')
% ylabel('angle [rad]')
% subplot(2,1,2)
% plot(u1(:,1),u1(:,2));
% title('Input: randomized square wave with ramp')
% xlabel('t [s]')
% ylabel('amplitude')
subplot(3,1,2)                                                                      % plot input and measured output data in one figure
plot(y1(:,1),y1(:,2));                                                      
hold on
plot(y(:,1),y(:,2));
legend('actuated disc','output disc','Location','northwest')
title('Output')
xlabel('t [s]')
ylabel('angle [rad]')
subplot(3,1,1)
plot(u1(:,1),u1(:,2));
title('Input: randomized square wave with ramp')
xlabel('t [s]')
ylabel('amplitude')
subplot(3,1,3)
plot(y(:,1),abs(y1(:,2)-y(:,2)));
title('Absolute difference between the two angles')
xlabel('t [s]')
ylabel('angle [rad]')

load('SineAmpl_0_1Freq_0_5Simtime_100.mat')
data_SineAmpl_0_1Freq_0_5Simtime_100 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);           % save inputs and outputs as iddata object to be used in n4sid function
Ts_SineAmpl_0_1Freq_0_5Simtime_100 = Ts;                                              % save sampling time to be used for plot
u_SineAmpl_0_1Freq_0_5Simtime_100 = u1(:,2);                                          % save u to be used for plot to compare

subplot(2,1,2)                                                                          % plot input and measured output data in one figure
hold on
plot(y(:,1),y(:,2));
plot(y1(:,1),y1(:,2));
legend('non-actuated disc','actuated disc')
title('Output')
subplot(2,1,1)
plot(u1(:,1),u1(:,2));
axis([0 100 -0.3 0.3])
title('Sine input')


load('SineAmpl_0_1Freq_1Simtime_100.mat')
data_SineAmpl_0_1Freq_1Simtime_100 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);           % save inputs and outputs as iddata object to be used in n4sid function
Ts_SineAmpl_0_1Freq_1Simtime_100 = Ts;                                              % save sampling time to be used for plot
u_SineAmpl_0_1Freq_1Simtime_100 = u1(:,2);                                          % save u to be used for plot to compare

figure                                                                      % plot input and measured output data in one figure
plot(y1(:,1),y1(:,2));                                                      
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('SineAmpl_0_1Freq_1Simtime_100')


load('SineAmpl_0_1Freq_2Simtime_100.mat')
data_SineAmpl_0_1Freq_2Simtime_100 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);           % save inputs and outputs as iddata object to be used in n4sid function
Ts_SineAmpl_0_1Freq_2Simtime_100 = Ts;                                              % save sampling time to be used for plot
u_SineAmpl_0_1Freq_2Simtime_100 = u1(:,2);                                          % save u to be used for plot to compare

figure                                                                      % plot input and measured output data in one figure
plot(y1(:,1),y1(:,2));                                                      
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('SineAmpl_0_1Freq_2Simtime_100')


load('SquareAmpl_0_1Freq_1Simtime_100.mat')
data_SquareAmpl_0_1Freq_1Simtime_100 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);           % save inputs and outputs as iddata object to be used in n4sid function
Ts_SquareAmpl_0_1Freq_1Simtime_100 = Ts;                                              % save sampling time to be used for plot
u_SquareAmpl_0_1Freq_1Simtime_100 = u1(:,2);                                          % save u to be used for plot to compare

figure                                                                      % plot input and measured output data in one figure
plot(y1(:,1),y1(:,2));                                                      
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('SquareAmpl_0_1Freq_1Simtime_100')





%% Determine model using subspace identification on RandomSquareAmpl_0_1Simtime_100
SYS4__RandomSquareAmpl_0_1Simtime_100 = n4sid(data_RandomSquareAmpl_0_1Simtime_100,4,...
    'DisturbanceModel','none');                                             % estimate model assuming order = 4 and no noise
SYS5__RandomSquareAmpl_0_1Simtime_100 = n4sid(data_RandomSquareAmpl_0_1Simtime_100,5,...
    'DisturbanceModel','none');
SYS6__RandomSquareAmpl_0_1Simtime_100 = n4sid(data_RandomSquareAmpl_0_1Simtime_100,6,...
    'DisturbanceModel','none');
SYS7__RandomSquareAmpl_0_1Simtime_100 = n4sid(data_RandomSquareAmpl_0_1Simtime_100,7,...
    'DisturbanceModel','none');
SYS8__RandomSquareAmpl_0_1Simtime_100 = n4sid(data_RandomSquareAmpl_0_1Simtime_100,8,...
    'DisturbanceModel','none');
SYS9__RandomSquareAmpl_0_1Simtime_100 = n4sid(data_RandomSquareAmpl_0_1Simtime_100,9,...
    'DisturbanceModel','none');
SYS10__RandomSquareAmpl_0_1Simtime_100 = n4sid(data_RandomSquareAmpl_0_1Simtime_100,10,...
    'DisturbanceModel','none');

%% Making some comparison plots
figure
lsim(SYS6__RandomSquareAmpl_0_1Simtime_100,u_RandomSquareRampSimtime_100,(0:Ts_RandomSquareRampSimtime_100:2000*Ts_RandomSquareRampSimtime_100))


%% calculate VAF's of different models

% VAF 4th order model
[y,~,~] = lsim(SYS4__RandomSquareAmpl_0_1Simtime_100,...
    u_RandomSquareRampSimtime_100,...
    (0:Ts_RandomSquareRampSimtime_100:2000*Ts_RandomSquareRampSimtime_100));
num = 0;
den = 0;
for i = 1:length(y)
    num = num + norm(y_RandomSquareRampSimtime_100(i) - y(i,2))^2;
    den = den + norm(y_RandomSquareRampSimtime_100(i))^2;
end
VAF(4) = (1-num/den)*100;
figure
hold on
plot(y_RandomSquareRampSimtime_100);
plot(y(:,2))
title('order 4 model')


% VAF 5th order model
[y,~,~] = lsim(SYS5__RandomSquareAmpl_0_1Simtime_100,...
    u_RandomSquareRampSimtime_100,...
    (0:Ts_RandomSquareRampSimtime_100:2000*Ts_RandomSquareRampSimtime_100));
num = 0;
den = 0;
for i = 1:length(y)
    num = num + norm(y_RandomSquareRampSimtime_100(i) - y(i,2))^2;
    den = den + norm(y_RandomSquareRampSimtime_100(i))^2;
end
VAF(5) = (1-num/den)*100;
figure
hold on
plot(y_RandomSquareRampSimtime_100);
plot(y(:,2))
title('order 5 model')

% VAF 6th order model
[y,~,~] = lsim(SYS6__RandomSquareAmpl_0_1Simtime_100,...
    u_RandomSquareRampSimtime_100,...
    (0:Ts_RandomSquareRampSimtime_100:2000*Ts_RandomSquareRampSimtime_100));
num = 0;
den = 0;
for i = 1:length(y)
    num = num + norm(y_RandomSquareRampSimtime_100(i) - y(i,2))^2;
    den = den + norm(y_RandomSquareRampSimtime_100(i))^2;
end
VAF(6) = (1-num/den)*100;
figure
hold on
plot(y_RandomSquareRampSimtime_100);
plot(y(:,2))
title('order 6 model')


% VAF 7th order model
[y,~,~] = lsim(SYS7__RandomSquareAmpl_0_1Simtime_100,...
    u_RandomSquareRampSimtime_100,...
    (0:Ts_RandomSquareRampSimtime_100:2000*Ts_RandomSquareRampSimtime_100));
num = 0;
den = 0;
for i = 1:length(y)
    num = num + norm(y_RandomSquareRampSimtime_100(i) - y(i,2))^2;
    den = den + norm(y_RandomSquareRampSimtime_100(i))^2;
end
VAF(7) = (1-num/den)*100;
figure
hold on
plot(y_RandomSquareRampSimtime_100);
plot(y(:,2))
title('order 7 model')

% VAF 8th order model
[y,~,~] = lsim(SYS8__RandomSquareAmpl_0_1Simtime_100,...
    u_RandomSquareRampSimtime_100,...
    (0:Ts_RandomSquareRampSimtime_100:2000*Ts_RandomSquareRampSimtime_100));
num = 0;
den = 0;
for i = 1:length(y)
    num = num + norm(y_RandomSquareRampSimtime_100(i) - y(i,2))^2;
    den = den + norm(y_RandomSquareRampSimtime_100(i))^2;
end
VAF(8) = (1-num/den)*100;
subplot(2,1,2)
hold on
plot(y_RandomSquareRampSimtime_100);
plot(y(:,2))
title('Output')
legend('second disc of real system','second disc of simulated system','Location','southwest')
xlabel('time [s]')
ylabel('angle [rad]')
axis([0 2000 -40 20])
subplot(2,1,1)
hold on
plot(u_RandomSquareRampSimtime_100);
title('Input')
xlabel('time [s]')
ylabel('amplitude')
axis([0 2000 -0.1 0.1])

% VAF 9th order model
[y,~,~] = lsim(SYS9__RandomSquareAmpl_0_1Simtime_100,...
    u_RandomSquareRampSimtime_100,...
    (0:Ts_RandomSquareRampSimtime_100:2000*Ts_RandomSquareRampSimtime_100));
num = 0;
den = 0;
for i = 1:length(y)
    num = num + norm(y_RandomSquareRampSimtime_100(i) - y(i,2))^2;
    den = den + norm(y_RandomSquareRampSimtime_100(i))^2;
end
VAF(9) = (1-num/den)*100;
figure
hold on
plot(y_RandomSquareRampSimtime_100);
plot(y(:,2))
title('order 9 model')

% VAF 10th order model
[y,~,~] = lsim(SYS10__RandomSquareAmpl_0_1Simtime_100,...
    u_RandomSquareRampSimtime_100,...
    (0:Ts_RandomSquareRampSimtime_100:2000*Ts_RandomSquareRampSimtime_100));
num = 0;
den = 0;
for i = 1:length(y)
    num = num + norm(y_RandomSquareRampSimtime_100(i) - y(i,2))^2;
    den = den + norm(y_RandomSquareRampSimtime_100(i))^2;
end
VAF(10) = (1-num/den)*100;
figure
hold on
plot(y_RandomSquareRampSimtime_100);
plot(y(:,2))
title('order 10 model')





