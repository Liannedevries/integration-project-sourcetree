clear all
close all
% SC42035 Integration Project {Flexlink Setup}

% Created by:
% Lianne De Vries 4349741
% Arjan Vonk      4219201

%% Load all experiments test data
% Legend:
% Data{1} = Step * Ramp from 0.05 - 0.1 (identification set)
% Data{2} = Step * Ramp from 0.05 - 0.1 (reference set)

Data{1} = load('SR_5_10_1.mat');
Data{2} = load('SR_5_10_2.mat');

% Create .iddData for experiments
for index = 1:length(Data)
    Data{index}.iddData = iddata(Data{index}.y1(:,2),Data{index}.u1(:,2),Data{index}.Ts);
end

%% Determine model using Subspace Identification 
% Create System from Experiment with SUB ID,
% SubID{x} = xth order system model created with subspace ID
SubID = cell(10,1);
IDset = Data{1};                               %Change Identification set here
REFset = Data{1};
REFset.t = REFset.y1(:,1);                     %Remove t entries from REFset inputs and outputs
REFset.y1 = REFset.y1(:,2);
REFset.u1 = REFset.u1(:,2);

for i = 1:length(SubID)
SubID{i} = n4sid(IDset.iddData,i,'DisturbanceModel','none');  %Create Subspace models 
end

VAF = zeros(10,1);
for i = 1:length(SubID)              %Calculate VAF for every x'th order model
   [ysim,~,~] = lsim(SubID{i},REFset.u1,REFset.t); %Do simulation
   num = 0;
   den = 0;
   for j = 1:length(ysim)
        num = num + norm(REFset.y1(j) - ysim(j))^2;
        den = den + norm(REFset.y1(j))^2;
   end
   VAF(i) = (1-num/den)*100;    
end

%% Make Closed Loop system for 8th Order model
% Due to the VAF of the 8th order model being the lowest, the 8th order
% model is going to be implemented to generate a LQR controller

order = 5;                          % Specify system order here
Qn = 1;                             % Specify Noise characteristics
Rn = 1;

G = SubID{order};
G = ss(G.A,G.B,G.C,0,-1);   %convert System to State Space Representation

[KSys,K,~,~] = kalman(G,Qn,Rn);  % K is Kalman Gain Matrix K

%% Create F for LQR controller 

% Specify Weighting Matrices Q (for states) and R (for input)
Qw = eye(order)*0.1     ;
Rw = 0.2            ;

F = generateLQR(G.A,G.B,Qw,Rw);      

%% Create Reference Tracking System
% Try to create a reference tracking system with input:
% u = -Fx + G*R
% This section tries to find the G-matrix that tracks performance
A = G.a;
B = G.b;
C = G.C;
D = G.D;
Dh = 0;
Ch = G.c;

%Full information feedback controller
Gcinv = Dh - (Ch - Dh*F)*inv(A-B*F)*B;
Gcontrol = 1/Gcinv;

% observer gain and controller gain using pole placement
p_controller = ones(1,order)*-2;
p_observer = ones(1,order)*-3;
Kobs = place(A,B,p_controller);
Lobs = place(A,B,p_observer);
