%% identifying the parameters of the DC-motor
clear
% % convert transfer function to SS
% syms K R b L J
% SYS = tf( [K] , [L*J R*J+L*b R*b+K^2 0]);
% [A B C D] = tf2ss(SYS);

% constants
m = 0.016;          % mass [kg] of weight used for experiment
r = 0.01;           % radius of axis [m] !ESTIMATE, STILL NEEDS TO BE MEASURED!
g = 9.81;           
Tm = g*m*r;         % torque on disc resulting from weight

%% load datasets
DCData{1} = load('DCmotor_with_weight.mat');
DCData{2} = load('DCmotor_with_weight2.mat');
DCData{3} = load('DCmotor_with_weight3.mat');

% determine derivative of theta
for i = 1:length(DCData)
    for k = 1:length(DCData{i}.y1)-1
        DCData{i}.dtheta(k) = (DCData{i}.y1(k+1,2) -...
            DCData{i}.y1(k,2))/DCData{i}.Ts;
    end
end

%% pretty soon a constant rotational speed is reached (can be seen in plots)
% This means that we can quite easily determine b: Tm = b*dtheta

for i = 1:length(DCData)
    DCData{i}.liny1 = DCData{i}.y1(500:1500,:);
    DCData{i}.lindtheta = DCData{i}.dtheta(500:1500);
end

% determine b
b = zeros(3,1);
for i = 1:length(DCData)
    DCData{i}.derivative_theta = (DCData{i}.liny1(1001,2) - DCData{i}.liny1(1,2))/...
        (DCData{i}.liny1(1001,1) - DCData{i}.liny1(1,1));
    b(i) = Tm/DCData{i}.derivative_theta;
end

%% now look at part of the data where the disc is still accelerating
% calculate angular acceleration
for i = 1:length(DCData)
    for k = 1:length(DCData{i}.dtheta)-1
        DCData{i}.alpha(k) = (DCData{i}.dtheta(k+1) -...
            DCData{i}.dtheta(k))/DCData{i}.Ts;
    end
end


dataset = 2;
figure
subplot(2,1,1)
hold on
plot(DCData{dataset}.y1(1:3000,1),DCData{dataset}.y1(1:3000,2))
plot(DCData{dataset}.y1(1:3000,1),DCData{dataset}.dtheta(1:3000))
subplot(2,1,2)
plot(DCData{dataset}.y1(1:2999,1),DCData{dataset}.alpha(1:2999))







