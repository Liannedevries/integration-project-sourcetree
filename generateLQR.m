%% LQR controller test script

function F = generateLQR(A,B,Q,R)

%Solve Ricatti Equation to get P
Binput = B/R*B';       %% If something is wrong with this function it's probably this line
P = are(A,Binput,Q);

F = -inv(R)*B'*P;