% WB59 - Karretje Project 2

% Afmeting Helling:
hmax=1.5
lh=5                                % lengte van de helling (m)
sh=7                                % totale lengte helling + vlakke stuk (m)
xh=sqrt(5^2+1.5^2)                  % horizontale afstand helling (m)
bh=2                                % vlakke stuk na de helling (m)
alfa= acos(hmax/5)                  % hoek tussen schuine en overstaande zijde van helling (rad) 

% Afmetingen onderdelen kar & constantes
d=0.09                              % diameter van wiel (m)
rw=0.045                            % straal van wiel (m)
a=0.15                              % afstand tussen vliegwiel-as en (loodrecht op) wiel-as (m)
mvlgwl=linspace(0,0.5,101)          % massa vliegwiel (kg)
rvlgwl=linspace(0,0.1,101)          % straal van vliegvlieg (m)
r2=linspace(0,0.1,101)              % aandrijfstraal van vliegwiel-as (m)
vmax=9.81*hmax*cos(alfa)            % maximale snelheid van karretje zonder enkele wrijving (m/s)
T=7/(pi*d)                          % aantal rotaties van Wiel, heenweg (dimensieloos)
t= T*pi*d + a                       % benodigde lengte van touwtje gewikkeld om vliegwiel-as (m)
mkar=linspace(0,0.5,101)+mvlgwl     % massa kar(met vliegwiel) (kg)
r=linspace(0,0.045,101)             % aandrijfstraal van van wiel-as (m)

% Energieberekening met wrijvingen en snelheid op elk punt
h=linspace(0,1.5,101)               % hoogte helling (m)
s1=linspace(0,5,101)                % afstand over de helling (m)
s2=linspace(0,2,101)                % afstand over vlakke stuk na helling (m)

g=9.81                              % valversnelling (m/s^2)
c=0.002                             % rolwrijvingscoefficient
Fn1=mkar*g*cos(alfa)                % normaalkracht voor het eerste schuine stuk (N)
Fn2=mkar*g                          % normaalkracht voor het tweede vlakke stuk (N)
Frw1=c*Fn1                          % rolwrijving voor het eerste schuine stuk (N)
Frw2=c*Fn2                          % rolwrijving voor het tweede vlakke stuk (N)

rho=1.293                           % luchtdichtheid (kg/m^3)
CwAf=0.5                            % luchtweerstandscoefficient x frontaal oppervlak (m^2)
Flw=0.5*rho.*vmax.^2.*CwAf          % luchtwrijving (N)

Epot=90*g.*hmax                  % potentiele energie van het karretje (J)
Ez=mkar.*g.*-h                      % zwaarte energie van het karretje (J)
Ew=(Frw1+Frw2+Flw).*s1              % totale wrijvingsenergie van het karretje (J)
v=abs(sqrt(Ez./(0.5.*mkar)))        % snelheid van het karretje (m/s)
w=v./r2                             % hoeksnelheid van vliegwiel-as (rad/s)
Evlgwl=0.5*(mvlgwl.*rvlgwl.^2).*w.^2% Kinetische Energie opgeslagen in vlgwl (J)
Ekin=0.5*mkar.*v.^2-Evlgwl          % kinetische energie van het karretje (zonder vliegwiel) (J)
%Epot=Ez+Ekin+Ew+Evlgwl              % energie-evenwichtsvergelijking van het karretje (J)



plot(s1,Epot)

%title('Opgeslagen energie in vliegwiel tegen de afgelegde afstand')
%xlabel('Afgelegde afstand (m)')
%ylabel('Opgeslagen Energie in vliegwiel (J)')



