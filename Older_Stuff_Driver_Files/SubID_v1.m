clear all

%% load test data, estimate order
% let s = 20, bigger than presumed order system
s=20;       % for hankel matrices
load('test_ramp_slope_0_1.mat')
UssN = hankel(u1(s:2*s,2),u1(2*s:3*s,2));
U0sN = hankel(u1(1:s,2),u1(s:2*s,2));
YssN = hankel(y(1:s,2),y(s:2*s,2));
data_ramp_slope_0_1 = iddata([y1(1:21,2) y(1:21,2)],u1(1:21,2),Ts); % only take values up until ampl = 0.1
Ts_ramp_slope_0_1 = Ts;

load('test_random_square_amp_0_1.mat')
data_random_square_amp_0_1 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
Ts_random_square_amp_0_1 = Ts;
UssN = hankel(u1(s:2*s,2),u1(2*s:3*s,2));
U0sN = hankel(u1(1:s,2),u1(s:2*s,2));
YssN = hankel(y(1:s,2),y(s:2*s,2));
u_random_square_amp_0_1 = u1(:,2);
% [R,Q] = rq([UssN; U0sN; YssN]);

figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('random_square_amp_0_1')

load('test_random_square_wave_with_ramp.mat')
data_random_square_wave_with_ramp = iddata([y1(1:101,2) y(1:101,2)],u1(1:101,2),Ts); % only take values up until ampl = 0.1
Ts_random_square_wave_with_ramp = Ts;
subplot(3,1,2)                                                                      % plot input and measured output data in one figure
plot(y1(:,1),y1(:,2));                                                      
hold on
plot(y(:,1),y(:,2));
legend('actuated disc','output disc','Location','northwest')
title('Output')
xlabel('t [s]')
ylabel('angle [rad]')
subplot(3,1,1)
plot(u1(:,1),u1(:,2));
title('Input: randomized square wave with ramp')
xlabel('t [s]')
ylabel('amplitude')
subplot(3,1,3)
plot(y(:,1),abs(y1(:,2)-y(:,2)));
title('Absolute difference between the two angles')
xlabel('t [s]')
ylabel('angle [rad]')


load('test_sin_amp_1_freq_0_1.mat')
data_sin_amp_1_freq_0_1 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
Ts_sin_amp_1_freq_0_1 = Ts;
figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('sin_amp_1_freq_0_1')

load('test_sin_amp_1_freq_0_5.mat')
data_sin_amp_1_freq_0_5 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
Ts_sin_amp_1_freq_0_5 = Ts;
figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('sin_amp_1_freq_0_5')

load('test_sin_amp_1_freq_1.mat')
data_sin_amp_1_freq_1 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
Ts_sin_amp_1_freq_1 = Ts;
figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('sin_amp_1_freq_1')

load('test_sin_amp_1_freq_2.mat')
data_sin_amp_1_freq_2 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
Ts_sin_amp_1_freq_2 = Ts;
figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('sin_amp_1_freq_2')

load('test_square_ampl_1_freq_0_5.mat')
data_square_amp_1_freq_0_5 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('square_amp_1_freq_0_5')
Ts_square_amp_1_freq_0_5 = Ts;

load('test_square_ampl_2_freq_0_5.mat')
data_square_amp_2_freq_0_5 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
Ts_square_amp_2_freq_0_5 = Ts;
figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('square_ampl_2_freq_0_5')

load('test_step.mat')
data_step_1 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
Ts_step_1 = Ts;
figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('step')

load('test_step_0_2.mat')
data_step_0_2 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);
Ts_step_0_2 = Ts;
figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('step_0_2')

%%
SYS4__random_square_amp_0_1 = n4sid(data_random_square_amp_0_1,4,'DisturbanceModel','none');
SYS6__random_square_amp_0_1 = n4sid(data_random_square_amp_0_1,6,'DisturbanceModel','none');
SYS10__random_square_amp_0_1 = n4sid(data_random_square_amp_0_1,10,'DisturbanceModel','none');

SYS4__random_square_wave_with_ramp = n4sid(data_random_square_wave_with_ramp,4,'DisturbanceModel','none');
SYS6__random_square_wave_with_ramp = n4sid(data_random_square_wave_with_ramp,6,'DisturbanceModel','none');
SYS10__random_square_wave_with_ramp = n4sid(data_random_square_wave_with_ramp,10,'DisturbanceModel','none');

figure
step(SYS6__random_square_wave_with_ramp)
figure
step(SYS6__random_square_amp_0_1)
figure
lsim(SYS6__random_square_amp_0_1,u_random_square_amp_0_1,...
    (0:Ts_random_square_amp_0_1:Ts_random_square_amp_0_1*1000))
figure
lsim(SYS6__random_square_wave_with_ramp,u_random_square_wave_with_ramp,...
    (0:Ts_random_square_wave_with_ramp:Ts_random_square_wave_with_ramp*100))
