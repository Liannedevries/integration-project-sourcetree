timestep = 0.01;
runtime = 600;
signalfrequency = 1;
voltage = 5;

clear x*;
fugiboard('CloseAll');
h=fugiboard('Open', 'mops1');
h.WatchdogTimeout = 2;
fugiboard('SetParams', h);
fugiboard('Write', h, 0, 0, [0 0]);  % dummy write to sync interface board
fugiboard('Write', h, 4+1, 1, [0 0]);  % get version, reset position, activate relay
data1 = fugiboard('Read', h);
model = bitshift(data1(1), -4);
version = bitand(data1(1), 15);
disp(sprintf('MOPS 1: FPGA setup %d,  version %d', model, version));
fugiboard('Write', h, 0, 1, [0 0]);  % end reset

g=fugiboard('Open', 'mops2');
g.WatchdogTimeout = 2;
fugiboard('SetParams', g);
fugiboard('Write', g, 0, 0, [0 0]);  % dummy write to sync interface board
fugiboard('Write', g, 4+1, 1, [0 0]);  % get version, reset position, activate relay
data2 = fugiboard('Read', g);
model = bitshift(data2(1), -4);
version = bitand(data2(1), 15);
disp(sprintf('MOPS 2: FPGA setup %d,  version %d', model, version));
fugiboard('Write', g, 0, 1, [0 0]);  % end reset

steps = runtime/timestep;
sigsteps = 1 / (2 * signalfrequency * timestep);

pause(0.5); % give relay some time to act

xstat = zeros(2,steps);
xreltime = zeros(2,steps);
xpos = zeros(2,steps);
tic;
bt = toc;
for X=1:steps
    if (mod(X, sigsteps) == 0)
        voltage = -voltage;
    end
    fugiboard('Write', h, 0, 1, [voltage 0.0]);
    fugiboard('Write', g, 0, 1, [0.0 0.0]);
    data1 = fugiboard('Read', h);
    data2 = fugiboard('Read', g);
    xstat(1,X) = data1(1);
    xreltime(1,X) = data1(2);
    xpos(1,X) = data1(3);
    xstat(2,X) = data2(1);
    xreltime(2,X) = data2(2);
    xpos(2,X) = data2(3);
    t = bt + (timestep * X);
    %t = toc + 0.005;
    while (toc < t); end;
end
toc;
fugiboard('Write', h, 0, 0, [0.0 0.0]);
fugiboard('Write', g, 0, 0, [0.0 0.0]);
figure(1); stairs(xpos'); ylabel('Position');