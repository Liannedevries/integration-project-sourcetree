clear all

%% load test data

load('test_random_square_amp_0_1.mat')
data_random_square_amp_0_1 = iddata([y1(:,2) y(:,2)],u1(:,2),Ts);           % save inputs and outputs as iddata object to be used in n4sid function
Ts_random_square_amp_0_1 = Ts;                                              % save sampling time to be used for plot
u_random_square_amp_0_1 = u1(:,2);                                          % save u to be used for plot to compare

figure                                                                      % plot input and measured output data in one figure
plot(y1(:,1),y1(:,2));                                                      
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('random_square_amp_0_1')


load('test_random_square_wave_with_ramp.mat')
data_random_square_wave_with_ramp = iddata([y1(1:101,2) y(1:101,2)],u1(1:101,2),Ts); % only take values up until ampl = 0.1
Ts_random_square_wave_with_ramp = Ts;
u_random_square_wave_with_ramp = u1(1:101,2);

figure
plot(y1(:,1),y1(:,2));
hold on
plot(y(:,1),y(:,2));
plot(u1(:,1),u1(:,2));
legend('y1','y','u1')
title('random_square_wave_with_ramp')


%%
SYS4__random_square_amp_0_1 = n4sid(data_random_square_amp_0_1,4,...
    'DisturbanceModel','none');                                             % estimate model assuming order = 4 and no noise
SYS6__random_square_amp_0_1 = n4sid(data_random_square_amp_0_1,6,...
    'DisturbanceModel','none');
SYS10__random_square_amp_0_1 = n4sid(data_random_square_amp_0_1,10,...
    'DisturbanceModel','none');

SYS4__random_square_wave_with_ramp = n4sid(data_random_square_wave_with_ramp,4,...
    'DisturbanceModel','none');
SYS6__random_square_wave_with_ramp = n4sid(data_random_square_wave_with_ramp,6,...
    'DisturbanceModel','none');
SYS10__random_square_wave_with_ramp = n4sid(data_random_square_wave_with_ramp,10,...
    'DisturbanceModel','none');

%%
figure
step(SYS6__random_square_wave_with_ramp)        % plot step response of extimated system
figure
step(SYS6__random_square_amp_0_1)
figure
% plot response of system estimated using data of random_square_amp_0_1 to input u_random_squae_amp_0_1 
lsim(SYS6__random_square_amp_0_1,u_random_square_amp_0_1,...
    (0:Ts_random_square_amp_0_1:Ts_random_square_amp_0_1*1000))
% plot response of system estimated using data of random_square_amp_0_1 to input u_random_square wave 
figure
lsim(SYS6__random_square_amp_0_1,u_random_square_wave_with_ramp,...
    (0:Ts_random_square_wave_with_ramp:Ts_random_square_wave_with_ramp*100))

%% plot response of system estimated using data of random_square_wave_with_ramp to input u_random_squre_wave_with_ramp 
figure
lsim(SYS6__random_square_wave_with_ramp,u_random_square_wave_with_ramp,...
    (0:Ts_random_square_wave_with_ramp:Ts_random_square_wave_with_ramp*100))
% plot response of system estimated using data of random_square_wave_with_ramp to input u_random_square_amp_0_1 
figure
lsim(SYS6__random_square_wave_with_ramp,u_random_square_amp_0_1,...
    (0:Ts_random_square_amp_0_1:Ts_random_square_amp_0_1*1000))

%% calculate VAF's of different models